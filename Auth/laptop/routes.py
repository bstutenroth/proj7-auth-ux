from flask import Flask
from flask import render_template, flash, redirect, url_for
from forms import LoginForm
from config import Config

app = Flask(__name__)
app.config.from_object(Config)

@app.route('/')
@app.route('/index')
def index():
    form = LoginForm()
    user = {'username': form.username.data}
    return render_template('index.html', title='Home', user=user)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        return render_template('calc.html')
    return render_template('login.html',  title='Sign In', form=form)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
