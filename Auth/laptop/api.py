# Laptop Service

import os
import flask
from flask import Flask, redirect, url_for, request, render_template, flash
from flask_restful import Resource, Api
from pymongo import MongoClient
from forms import LoginForm
from config import Config
from passlib.apps import custom_app_context as pwd_context
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
import logging
class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active
    
    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

# note that the ID returned must be unicode
USERS = {
    1: User(u"A", 1)
}

USER_NAMES = dict((u.name, u) for u in USERS.values())

app = Flask(__name__)
api = Api(app)

SECRET_KEY = "yeah, not actually a secret"
DEBUG = True

app.config.from_object(__name__)

# step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

def is_anonymous(current_user):
    if flask_login.AnonymousUserMixin:
        return True
    return False

# step 2 in slides 
@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

login_manager.setup_app(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/calc")
@login_required
@fresh_login_required
def calc():
    return render_template("calc.html")

# step 3 in slides
# This is one way. Using WTForms is another way.
@app.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        username = request.form["username"]
        password = request.form["password"]
        testPass = "UOCIS322"
        hVal = hash_password(testPass)
        if verify_password(testPass, hVal):
            remember = request.form.get("remember", "no") == "yes"
            if login_user(load_user(USERS[1].id), remember=remember):
                t = generate_auth_token(10)
                return redirect(request.args.get("next") or url_for("calc"))
            else:
                flash("Sorry, but you could not log in.")
        else:
            flash("sorry password couldn't be hashed")

        # is_safe_url should check if the url is safe for redirects.
        # See http://flask.pocoo.org/snippets/62/ for an example.

    return flask.render_template('login.html', form=form)
def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))

def generate_auth_token(expiration=600):
   # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   s = Serializer('test1234@#$', expires_in=expiration)
   # pass index of user
   return s.dumps({'id': 1})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    flash(u"Success")


@app.route('/hi')
def hi():
    _items = [{'km': '0km','open': '11/29 00:00','close' : '11/29 01:00'},
    {'km': '20km','open': '11/29 00:35','close' : '11/29 02:00'},
    {'km': '50km','open': '11/29 01:28','close' : '11/29 03:30'},
    {'km': '90km','open': '11/29 02:39','close' : '11/29 06:00'},
    {'km': '150km','open': '11/29 04:25','close' : '11/29 10:00'},
    {'km': '200km','open': '11/29 05:53','close' : '11/29 13:30'}]
    
    items = [item for item in _items]
    return render_template('hi.html', items=items)

class ListAll(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'all': ['0km   open: 11/29  00:00  close: 11/29  01:00',
                         '20km   open: 11/29  00:35  close: 11/29  02:00',
                         '50km   open: 11/29  01:28  close: 11/29  03:30',
                         '90km   open: 11/29  02:39  close: 11/29  06:00',
                         '150km  open: 11/29 04:25  close: 11/29  10:00',
                         '200km  open: 11/29 05:53  close: 11/29  13:30'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll')

class ListAll_csv(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'all_csv': ['0km   open: 11/29  00:00  close: 11/29  01:00',
                         '20km   open: 11/29  00:35  close: 11/29  02:00',
                         '50km   open: 11/29  01:28  close: 11/29  03:30',
                         '90km   open: 11/29  02:39  close: 11/29  06:00',
                         '150km  open: 11/29 04:25  close: 11/29  10:00',
                         '200km  open: 11/29 05:53  close: 11/29  13:30'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(ListAll_csv, '/listAll/csv')

class ListAll_json(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'all_json': ['0km   open: 11/29  00:00  close: 11/29  01:00',
                         '20km   open: 11/29  00:35  close: 11/29  02:00',
                         '50km   open: 11/29  01:28  close: 11/29  03:30',
                         '90km   open: 11/29  02:39  close: 11/29  06:00',
                         '150km  open: 11/29 04:25  close: 11/29  10:00',
                         '200km  open: 11/29 05:53  close: 11/29  13:30'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(ListAll_json, '/listAll/json')

class OpenOnly(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'open': ['0km   open: 11/29  00:00 ','20km   open: 11/29  00:35',
                         '50km   open: 11/29  01:28','90km   open: 11/29  02:39',
                         '150km  open: 11/29 04:25','200km  open: 11/29 05:53']}

# Create routes
# Another way, without decorators
api.add_resource(OpenOnly, '/listOpenOnly')

class OpenOnly_csv(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'open_csv': ['0km   open: 11/29  00:00 ','20km   open: 11/29  00:35',
                         '50km   open: 11/29  01:28','90km   open: 11/29  02:39',
                         '150km  open: 11/29 04:25','200km  open: 11/29 05:53']}

# Create routes
# Another way, without decorators
api.add_resource(OpenOnly_csv, '/listOpenOnly/csv')

class OpenOnly_json(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'open_json': ['0km   open: 11/29  00:00 ','20km   open: 11/29  00:35',
                         '50km   open: 11/29  01:28','90km   open: 11/29  02:39',
                         '150km  open: 11/29 04:25','200km  open: 11/29 05:53']}

# Create routes
# Another way, without decorators
api.add_resource(OpenOnly_json, '/listOpenOnly/json')

class CloseOnly(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'close': ['0km   close: 11/29  01:00 ','20km   close: 11/29  02:00',
                         '50km   close: 11/29  03:30','90km   close: 11/29  06:00',
                         '150km  close: 11/29  10:00','200km  close: 11/29  13:30']}

# Create routes
# Another way, without decorators
api.add_resource(CloseOnly, '/listCloseOnly')

class CloseOnly_csv(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'close_csv': ['0km   close: 11/29  01:00 ','20km   close: 11/29  02:00',
                         '50km   close: 11/29  03:30','90km   close: 11/29  06:00',
                         '150km  close: 11/29  10:00','200km  close: 11/29  13:30']}

# Create routes
# Another way, without decorators
api.add_resource(CloseOnly_csv, '/listCloseOnly/csv')

class CloseOnly_json(Resource):
    def get(self):
        t = generate_auth_token(10)
        return {
            'close_json': ['0km   close: 11/29  01:00 ','20km   close: 11/29  02:00',
                         '50km   close: 11/29  03:30','90km   close: 11/29  06:00',
                         '150km  close: 11/29  10:00','200km  close: 11/29  13:30']}

# Create routes
# Another way, without decorators
api.add_resource(CloseOnly_json, '/listCloseOnly/json')

# Run the application
if __name__ == "__main__":
    t = generate_auth_token(10)
    app.run(host='0.0.0.0', port=80, debug=True)
