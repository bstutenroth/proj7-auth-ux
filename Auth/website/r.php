<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $r = $obj->r;
            foreach ($r as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
    </body>
</html>
