<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	          $all = $obj->all;
            foreach ($all as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $all_csv = $obj->$all_csv;
            echo implode(",", $all_csv);
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
            $all_json= $obj->all_json;
            foreach ($all_json as $l) {
                echo JSON.parse($l);
            }
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $open = $obj->open;
            foreach ($open as $l) {
                echo "<li>$l</li>";
            }
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $open_csv = $obj->open_csv;
            echo implode(",", $open_csv);
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
            $open_json= $obj->open_json;
            foreach ($open_json as $l) {
                echo JSON.parse($l);
            }
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $close = $obj->close;
            foreach ($close as $l) {
                echo "<li>$l</li>";
            }
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $close_csv = $obj->close_csv;
            echo implode(",", $close_csv);
            ?>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
	          $close_json= $obj->close_json;
            foreach ($close_json as $l) {
                echo JSON.parse($l);
            }
            ?>
    </body>
</html>
